require 'fileutils'

@project_name = 'second-project'
@rails_demo_path = './rails-demo'


def copy_files(from_here, to_there)
	Dir.mkdir('./'+to_there) unless Dir.exist?('./'+to_there)
	Dir[from_here+'/*'].each do |file|
		new = file.gsub(from_here, './'+to_there)
		FileUtils.cp(file, new)
	end
end

def destination(project, filename)
	"./#{project}/#{filename}"
end


def prepare_deploy(project, filename)
	location = destination(project, filename)
	deploy_file = File.open(location, 'w+')
	deploy_file.each do |line|
		line.gsub()
	end

end

def prepare_conf

end

def prepare_init

end

def prepare_unicorn
	prepare_conf
	prepare_init
end

begin
# 	copy_files @rails_demo_path, @project_name
	prepare_deploy(@project_name, 'deploy.rb')
# 	prepare_unicorn
end