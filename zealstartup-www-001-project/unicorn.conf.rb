# set path to application
app_path = File.expand_path(File.join(File.dirname(__FILE__), '../../'))
working_directory File.join(app_path, 'current')

project = 'zealstartup-www-001-project'

#listen '127.0.0.1:4000'  # For testing

# Set up socket location
listen File.join(app_path, "shared/unicorn.#{project}.sock"), :backlog => 64

puts "listening to:#{File.join(app_path, "shared/unicorn.#{project}.sock")}"

# Set unicorn options
worker_processes 4
timeout 30

# Logging
stderr_path File.join(app_path, 'current/log/unicorn_stderr.log')
stdout_path File.join(app_path, 'current/log/unicorn_stdout.log')

# Set master PID location
pid File.join(app_path, "shared/unicorn.#{project}.pid")